# Simple dashboard

## Requirements
* Node.js. I'm using v4.4.1
* NPM. My version is 3.10.5
* Python2 required to build sass files 

## Dev stack
* [Webpack](http://webpack.github.io/) for building app
* [Express](https://expressjs.com/) server with webpack dev server middleware
* [Babel](http://babeljs.io/) for transform to ES5 
* [React](https://facebook.github.io/react/) view layer
* [Redux](https://github.com/reactjs/redux) architecture
* [Redux-saga](https://github.com/yelouafi/redux-saga) to handle io operations
* [React-toolbox](http://react-toolbox.com/) - material design components
* [Chartjs](http://www.chartjs.org/) for bar diagram
* [Tape](https://github.com/substack/tape) testing framework

## Few words about architecture
Redux may looks very strange compared with familiar MV* architecture and may take some time to understanding. 
So let me save your time and describe it in a few words.
Redux is:
* Actions - a plain js objects which consists of `type` (constant string) and payload.
* Reducers - a pure functions which generate new state based on current state and action.
* Store - an immutable tree which represent our application.

Views has a two types:
* Components - the pure functions which transform state to a representation tree (React model in our case)
* Containers - the components connected to Redux `store` using util function `connect` from `react-redux`. It just 
create a higher order component which wrap `compoenent` and propagate data from store via props.
 
Data flow:
Like a Flux. View dispatch actions (see `mapDispatchToProps` in containers). Action propagate to reducers. 
Reducers returns a new state. Store replace state and trigger event. View listened the event and updated.
 
Async actions:
In some cases you want to do something 'dirty' :simple_smile: for example send a ajax request. The Redux
principal is _use pure functions where you can_. So common way to do it is using redux `middleware`.    
Middleware works same as middleware for express server and wrap the store. There are a few types of middleware used 
for handling async actions by I would prefer a `redux-saga`. It starts on the page load and dispatched a events when
the result received.  


## Install

```sh
npm install
```


## Build to 'dist' directory

```sh
npm run build
```


## Run tests

```sh
npm run test
```


## Start dev server

```sh
npm run start
```


## Known bugs
* IE11 styles for measures list


## TODO list 
* Increase tests coverage
* Responsible markup and media queries; add scrolls to lists of dimensions and measures.  
* CSS modules and using themes; move styles and components to folder. 
* Client side routing. To save selected measure and dimensions in url.
* Error handling
* i18n
* HMR
* Redux Dev Tool
* Linting
* Common redux problem - create selectors resistant to changes in the store structure
* Huge bundle
