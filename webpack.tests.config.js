const path = require('path');

module.exports = {

  devtool: 'inline-source-map',

  resolve: {
    extensions: ['', '.js'],
  },

  resolveLoader: {
    modulesDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extensions: ['', '.js'],
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        loader: 'null',
      },
      {
        test: /\.json$/,
        loader: 'json',
        exclude: /node_modules/,
      },
    ],
  },

  plugins: [],
};
