import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import createLogger from 'redux-logger';
import rootSaga from  './sagas';
import rootReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger();

export default createStore(rootReducer,
  applyMiddleware(
    sagaMiddleware,
    loggerMiddleware //TODO: only for dev
  ));

sagaMiddleware.run(rootSaga);
