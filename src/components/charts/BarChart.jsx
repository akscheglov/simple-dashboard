import React, { PropTypes } from 'react';
import { Panel } from 'react-toolbox';
import { SeriesType } from './types';
import { Bar } from 'react-chartjs-2';
import styles from './styles';

/**
 * Version needed for a hot fix. I prefer version for monkey patch of Bar.shouldComponentUpdate.
 * Unable to deselect last by order dimension.
 * For example if dimensions list is ['Mercury', 'Mars'] dimension 'Mars' cannot be deselected.
 * Bug in the react-chartjs-2 library in the utils/deepEqual:
 * for (var i = 0; i < keysA.length; i++) {
 *   if (!hasOwnProperty.call(objB, keysA[i])) {
 *      return false;
 *   }
 * }
 * But what about keysA.length < keysB.length? ^^
 * Without version live circle method Bar.shouldComponentUpdate returns false in our case.
 */
let version = 0;

/**
 * @param {ViewModel~Series[]} series - The data to display
 */
function getData(series) {
  return {
    labels: series.map(item => item.x),
    datasets: [
      {
        data: series.map(item => item.y),
      },
    ],
    version: version++,
  };
}

function getOptions(title) {
  return {
    legend: {
      display: false,
    },
    title: {
      display: true,
      text: title,
      fontSize: 24,
    },
    responsive: true,
  };
}

/**
 * @param {string} title - The chart label
 * @param {ViewModel~Series[]} series - The data to display
 */
export function BarChart({ title, series }) {
  const data = getData(series);
  const options = getOptions(title);

  return (
    <Panel className={styles.chart} >
      {
        series.length &&
        <Bar
          data={data}
          options={options}
        /> ||
        <div className={styles.selectDimensions} >No dimensions selected</div>
      }
    </Panel>
  );
}

BarChart.propTypes = {
  title: PropTypes.string.isRequired,
  series: PropTypes.arrayOf(SeriesType).isRequired,
};
