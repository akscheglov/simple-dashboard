import { PropTypes } from 'react';

/**
 * Data series model. Data series is the {x,y} pair where x is dimension and y is value.
 * @typedef {Object} ViewModel~Series
 * @property {string} x
 * @property {string|number} y
 */

export const SeriesType = PropTypes.shape({
  x: PropTypes.string.isRequired,
  y: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
}); 
