import React, { PropTypes } from 'react';
import { Layout } from 'react-toolbox';
import { BarChart, SeriesType } from './charts/BarChart';
import { Measures, MeasureType } from './measures/Measures';
import { Dimensions, DimensionType } from './dimensions';

//TODO: add loader; handle no data and errors
export function Dashboard({ isFetching, title, series, measures, dimensions, onMeasureSelect, onDimensionClick }) {
  if (isFetching)
    return null;

  return (
    <Layout>
      <Measures
        measures={measures}
        onClick={onMeasureSelect}
      />

      <BarChart
        title={title}
        series={series}
      />

      <Dimensions
        dimensions={dimensions}
        onChange={onDimensionClick}
      />
    </Layout>
  )
}

Dashboard.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  series: PropTypes.arrayOf(SeriesType).isRequired,
  measures: PropTypes.arrayOf(MeasureType).isRequired,
  dimensions: PropTypes.arrayOf(DimensionType).isRequired,
  onMeasureSelect: PropTypes.func.isRequired,
  onDimensionClick: PropTypes.func.isRequired,
};
