import { PropTypes } from 'react';

/**
 * Dimension model.
 * @typedef {Object} ViewModel~Dimension
 * @property {string} label
 * @property {boolean} isSelected
 */

export const DimensionType = PropTypes.shape({
  label: PropTypes.string.isRequired,
  isSelected: PropTypes.bool.isRequired,
});
