import React, { PropTypes } from 'react';
import { Panel } from 'react-toolbox';
import { List, ListCheckbox, ListSubHeader } from 'react-toolbox/lib/list';
import { DimensionType } from './types';
import styles from './styles.sass';

/**
 * @param {ViewModel~Dimension[]} dimensions - Dimensions list to display.
 * @param {function} onChange - Callback will called on dimension state changed.
 */
//TODO: handle no dimensions; add select/deselect all
export function Dimensions({ dimensions, onChange }) {
  return (
    <Panel className={styles.dimensions} >
      <List>
        <ListSubHeader caption='Select dimensions' />
        {
          dimensions.map(item =>
            <ListCheckbox
              key={item.label}
              checked={item.isSelected}
              caption={item.label}
              onChange={onChange.bind(null, item.label)}
            />
          )
        }
      </List>
    </Panel>
  )
}

Dimensions.propTypes = {
  dimensions: PropTypes.arrayOf(DimensionType).isRequired,
  onChange: PropTypes.func.isRequired,
};
