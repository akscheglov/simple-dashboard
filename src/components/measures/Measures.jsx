import React, { PropTypes } from 'react';
import { Panel } from 'react-toolbox';
import { List, ListItem, ListSubHeader } from 'react-toolbox/lib/list';
import { MeasureType } from './types';
import styles from './styles.sass';

/**
 * @param {ViewModel~Measure[]} measures - The list of measures to display.
 * @param {function} onClick - The callback will called when measure clicked.
 */
//TODO: handle no measures
export function Measures({ measures, onClick }) {
  return (
    <Panel className={styles.measures} >
      <List selectable ripple className={styles.measures} >
        <ListSubHeader caption='Select measure' />
        {
          measures.map(item =>
            <ListItem
              key={item.id}
              onClick={onClick.bind(null, item.id)}
              caption={item.label}
              className={item.isSelected ? styles.selected : ''}
            />
          )
        }
      </List>
    </Panel>
  )
}

Measures.propTypes = {
  measures: PropTypes.arrayOf(MeasureType).isRequired,
  onClick: PropTypes.func.isRequired,
};
