import { PropTypes } from 'react';

/**
 * Measure model.
 * @typedef {Object} ViewModel~Measure
 * @property {string} id
 * @property {string} label
 * @property {boolean} isSelected
 */

export const MeasureType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  isSelected: PropTypes.bool.isRequired,
});
