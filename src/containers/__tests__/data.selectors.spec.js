import test from 'tape';
import {
  dataDefinitionSelector,
  labelSelector,
  columnsDefinitionSelector,
  seriesSelector,
  measuresSelector,
  dimensionsSelector,
} from '../data.selectors';

const createState = () => {
  return require('./data.json');
};

test('dataDefinitionSelector result when selected measure exists', (assert) => {
  const state = createState();
  const expected = state.data.values[2];
  const actual = dataDefinitionSelector(state);

  assert.equal(actual, expected,
    'dataDefinitionSelector() should return definition which id equals state.dashboard.measure');

  assert.end();
});

test('dataDefinitionSelector result when selected measure not exists', (assert) => {
  const state = { ...createState(), dashboard: { measure: null } };
  const expected = { col: {}, v: [] };
  const actual = dataDefinitionSelector(state);

  assert.deepEqual(actual, expected,
    'dataDefinitionSelector() should return default definition');

  assert.end();
});

test('labelSelector result when selected measure exists', (assert) => {
  const state = createState();
  const expected = 'Diameter (km)';
  const actual = labelSelector(state);

  assert.equal(actual, expected,
    'labelSelector() should return label of data definition which id equals state.dashboard.measure');

  assert.end();
});

test('labelSelector result when selected measure not exists', (assert) => {
  const state = { ...createState(), dashboard: { measure: null } };
  const expected = '';
  const actual = labelSelector(state);

  assert.equal(actual, expected,
    'labelSelector() should return default value');

  assert.end();
});

test('columnsDefinitionSelector result when definition exists', (assert) => {
  const state = createState();
  const expected = state.data.values[0];
  const actual = columnsDefinitionSelector(state);

  assert.equal(actual, expected,
    'columnsDefinitionSelector() should return first element from state.data.values');

  assert.end();
});

test('columnsDefinitionSelector result when definition not exists', (assert) => {
  const state = { ...createState(), data: { values: [] } };
  const expected = { col: {}, v: [] };
  const actual = columnsDefinitionSelector(state);

  assert.deepEqual(actual, expected,
    'columnsDefinitionSelector() should return default value');

  assert.end();
});

test('seriesSelector result when definition exists', (assert) => {
  const state = createState();
  const expected = [{ x: 'Mercury', y: '4879', }, { x: 'Venus', y: '12104', }];
  const actual = seriesSelector(state);

  assert.deepEqual(actual, expected,
    'seriesSelector() should return key-value pairs where key is dimension and value is related measure value');

  assert.end();
});

test('seriesSelector result when some of dimensions excluded', (assert) => {
  const state = { ...createState(), dashboard: { measure: 'diameter', excludedDimensions: ['Mercury'] } };
  const expected = [{ x: 'Venus', y: '12104', }];
  const actual = seriesSelector(state);

  assert.deepEqual(actual, expected,
    'seriesSelector() should return key-value pairs without excluded dimensions');

  assert.end();
});

test('seriesSelector result when measure not selected', (assert) => {
  const state = { ...createState(), dashboard: { measure: '' } };
  const expected = [];
  const actual = seriesSelector(state);

  assert.deepEqual(actual, expected,
    'seriesSelector() should return empty list');

  assert.end();
});

test('measuresSelector result', (assert) => {
  const state = createState();
  const expected = [
    { id: 'mass', label: 'Mass (10^24kg)', isSelected: false },
    { id: 'diameter', label: 'Diameter (km)', isSelected: true }
  ];
  const actual = measuresSelector(state);

  assert.deepEqual(actual, expected,
    'measuresSelector() should returns list measures without first element of state.data.values');

  assert.end();
});

test('dimensionsSelector result when some of dimensions excluded', (assert) => {
  const state = { ...createState(), dashboard: { excludedDimensions: ['Mercury'] } };
  const expected = [{ label: 'Mercury', isSelected: false, }, { label: 'Venus', isSelected: true, }];
  const actual = dimensionsSelector(state);

  assert.deepEqual(actual, expected,
    'dimensionsSelector() should return list of keys of state.data.values[0] with unselected excludedDimensions');

  assert.end();
});
