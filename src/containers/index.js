import { connect } from 'react-redux';
import { Dashboard as Component } from '../components';
import { selectMeasure, toggleDimension } from '../actions';
import { seriesSelector, measuresSelector, labelSelector, dimensionsSelector } from './data.selectors';

const mapStateToProps = (state) => {
  return {
    isFetching: state.data.isFetching,
    title: labelSelector(state),
    series: seriesSelector(state),
    measures: measuresSelector(state),
    dimensions: dimensionsSelector(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onMeasureSelect: (measure) => {
      dispatch(selectMeasure(measure));
    },
    onDimensionClick: (dimension) => {
      dispatch(toggleDimension(dimension));
    },
  }
};

export const Dashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);
