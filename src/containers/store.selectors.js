import { compose } from 'redux';

/**
 * @param {StoreModel} state
 * @return {StoreModel~ViewModel}
 */
export function dashboardSelector(state) {
  return state.dashboard;
}

/**
 * @param {StoreModel} state
 * @returns {StoreModel~DataModel}
 */
export function dataSelector(state) {
  return state.data;
}

/**
 * @param {StoreModel} state
 * @return {string}
 */
export function selectedMeasureSelector(state) {
  return compose(
    (dashboard) => dashboard.measure,
    dashboardSelector
  )(state);
}

/**
 * @param {StoreModel} state
 * @return {string[]}
 */
export function excludedDimensionsSelector(state) {
  return compose(
    (dashboard) => dashboard.excludedDimensions,
    dashboardSelector
  )(state);
}

/**
 * @param {StoreModel} state
 * @return {StoreModel~DataModel~DataDefinition[]}
 */
export function dataValuesSelector(state) {
  return compose(
    (data) => data.values,
    dataSelector
  )(state);
}
