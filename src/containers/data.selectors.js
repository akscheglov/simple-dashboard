import zip from 'lodash/zip';
import find from 'lodash/find';
import includes from 'lodash/includes';
import { createSelector } from 'reselect'
import {
  selectedMeasureSelector,
  dataValuesSelector,
  excludedDimensionsSelector
} from './store.selectors';

/**
 * Selectors allows to cash a results. As soon as we are using pure functions the output is determinate and
 * we can use high level optimisations in shouldComponentUpdate method of react Component.
 * So when we using selectors the component will update if only the data model is changed.
 */

/**
 * Used for render optimisations.
 * @type {{col: {}, v: Array}}
 */
const defaultDefinition = { col: {}, v: [] };

/**
 * 
 *
 * @param {StoreModel~DataModel~DataDefinition} columnsDefinition
 * @param {StoreModel~DataModel~DataDefinition} dataDefinition
 * @param {string[]} excludedDimensions
 * @returns {ViewModel~Series[]}
 */
function getSeries(columnsDefinition, dataDefinition, excludedDimensions) {
  return zip(columnsDefinition.v, dataDefinition.v)
    .filter(item => !includes(excludedDimensions, item[0]))
    .map(item => {
      return {
        x: item[0],
        y: item[1],
      };
    });
}

/**
 * Create dimensions view models.
 * 
 * @param {StoreModel~DataModel~DataDefinition} columnsDefinition
 * @param {string[]} excludedDimensions
 * @returns {ViewModel~Dimension[]}
 */
function getDimensions(columnsDefinition, excludedDimensions) {
  return columnsDefinition.v.map(item => {
    return {
      label: item,
      isSelected: !includes(excludedDimensions, item),
    };
  });
}

/**
 * Create measures view model.
 * 
 * @param {StoreModel~DataModel~DataDefinition[]} values
 * @param {string} selectedMeasure
 * @returns {ViewModel~Measure[]}
 */
function getMeasures(values, selectedMeasure) {
  return values.slice(1).map(item => {
    return {
      id: item.col.id,
      label: item.col.label,
      isSelected: selectedMeasure === item.col.id,
    };
  });
}

export const dataDefinitionSelector = createSelector(
  selectedMeasureSelector,
  dataValuesSelector,
  (selectedMeasure, values) => find(values, (item) => item.col.id === selectedMeasure) || defaultDefinition
);

export const labelSelector = createSelector(
  dataDefinitionSelector,
  (dataDefinition) => dataDefinition.col.label || ''
);

export const columnsDefinitionSelector = createSelector(
  dataValuesSelector,
  (values) => values[0] || defaultDefinition
);

export const seriesSelector = createSelector(
  selectedMeasureSelector,
  columnsDefinitionSelector,
  dataDefinitionSelector,
  excludedDimensionsSelector,
  (selectedMeasure, columnsDefinition, dataDefinition, excludedDimensions) =>
    selectedMeasure ? getSeries(columnsDefinition, dataDefinition, excludedDimensions) : []
);

export const measuresSelector = createSelector(
  dataValuesSelector,
  selectedMeasureSelector,
  getMeasures
);

export const dimensionsSelector = createSelector(
  columnsDefinitionSelector,
  excludedDimensionsSelector,
  getDimensions
);
