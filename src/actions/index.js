/**
 * Action type used for handling a dashboard data received from data source.
 * @type {string}
 */
export const RECEIVE_DASHBOARD_DATA = 'RECEIVE_DASHBOARD_DATA';

/**
 * Action type used to indicate that dashboard data start fetching.
 * @type {string}
 */
export const REQUEST_DASHBOARD_DATA = 'REQUEST_DASHBOARD_DATA';

/**
 * Action type used to select specified measure.
 * @type {string}
 */
export const SELECT_MEASURE = 'SELECT_MEASURE';

/**
 * Action type used to include or exclude specified data series.
 * @type {string}
 */
export const TOGGLE_DIMENSION = 'TOGGLE_DIMENSION';


/**
 * Create a {@link RECEIVE_DASHBOARD_DATA} action.
 * @param {Object} data - The dashboard data which was received.
 * @returns {{type: string, data: *}} - An action of {@link RECEIVE_DASHBOARD_DATA} type and {@param data} payload.
 */
export function receiveDashboardData(data) {
  return {
    type: RECEIVE_DASHBOARD_DATA,
    data,
  }
}

/**
 * Create a {@link REQUEST_DASHBOARD_DATA} action.
 * @returns {{type: string}} - An action of {@link REQUEST_DASHBOARD_DATA} type.
 */
export function requestDashboardData() {
  return {
    type: REQUEST_DASHBOARD_DATA,
  }
}

/**
 * Create a {@link SELECT_MEASURE} action.
 * @param {string} measure - The measure to select.
 * @returns {{type: string, measure: string}} - An action of {@link SELECT_MEASURE} type and {@param measure} payload.
 */
export function selectMeasure(measure) {
  return {
    type: SELECT_MEASURE,
    measure,
  }
}

/**
 * Create a {@link TOGGLE_DIMENSION} action.
 * @param {string} dimension - Dimension to include or exclude.
 * @returns {{type: string, dimension: string}} - An action of {@link TOGGLE_DIMENSION} type and {@param dimension} payload.
 */
export function toggleDimension(dimension) {
  return {
    type: TOGGLE_DIMENSION,
    dimension,
  }
}
