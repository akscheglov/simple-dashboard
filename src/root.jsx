import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import { Dashboard } from './containers';

export default () => (
  <Provider store={store} >
    <Dashboard />
  </Provider>
);
