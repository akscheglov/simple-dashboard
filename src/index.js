import React from 'react';
import ReactDOM from 'react-dom';
import RootContainer from './root';

ReactDOM.render(<RootContainer />, document.getElementById('root'));
