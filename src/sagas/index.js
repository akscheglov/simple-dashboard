import { put, call } from 'redux-saga/effects';
import { receiveDashboardData, requestDashboardData, selectMeasure } from '../actions';
import { valuesSelector } from '../reducers/data';

/**
 * This saga should be run on page load to request dashboard data.
 * Dispatched {@link requestDashboardData} action before request,
 * {@link receiveDashboardData} and {@link selectMeasure} actions after data received.
 */
export default function* initDashboardSaga() {
  try {
    yield put(requestDashboardData());
    const response = yield call(fetch, `//${API_HOST_NAME}/api/dashboard/data`, {});
    /**
     * @type {{ col: StoreModel~DataModel~DataDefinition[] }}
     */
    const data = yield response.json();
    const values = valuesSelector(data);
    const defaultValue = values.length > 1 ? values[1].col.id : '';
    yield [put(receiveDashboardData(data)), put(selectMeasure(defaultValue))];
  } catch (e) {
    /* TODO: handle error */
    console.error(e);
  }
};
