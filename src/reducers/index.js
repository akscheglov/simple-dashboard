import { combineReducers } from 'redux';
import data from './data';
import dashboard from './dashboard';

/**
 * Store model.
 * @typedef {Object} StoreModel
 * @property {StoreModel~DataModel} data
 * @property {StoreModel~ViewModel} dashboard
 */


export default combineReducers({
  data,
  dashboard,
});
