import {
  SELECT_MEASURE,
  TOGGLE_DIMENSION,
} from '../actions';

/**
 * Data definition model.
 * @typedef {Object} StoreModel~ViewModel
 * @property {string} measure - Currently selected measure.
 * @property {string[]} excludedDimensions - Dimensions which should be excluded from graph
 */

/**
 * Reducer to handle a view actions.
 * @param {StoreModel~ViewModel} state
 * @param action
 */
export default function dashboard(state = {
  measure: '',
  excludedDimensions: [],
}, action) {
  switch (action.type) {
    case SELECT_MEASURE:
      return { ...state, measure: action.measure, };

    case TOGGLE_DIMENSION:
      const excluded = state.excludedDimensions;
      const index = excluded.indexOf(action.dimension);

      let dimensions;
      if (index >= 0) {
        dimensions = [
          ...excluded.slice(0, index),
          ...excluded.slice(index + 1),
        ];
      } else {
        dimensions = [
          ...excluded,
          action.dimension,
        ];
      }

      return { ...state, excludedDimensions: dimensions, };

    default:
      return state;
  }
}
