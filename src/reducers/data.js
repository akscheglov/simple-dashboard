import {
  RECEIVE_DASHBOARD_DATA,
  REQUEST_DASHBOARD_DATA,
} from '../actions';

/**
 * Data definition model.
 * @typedef {Object} StoreModel~DataModel~DataDefinition~DataItem
 * @property {string} id - The unique id of the data item.
 * @property {string} category - The category of the data item: dimension, measures or ..
 * @property {string} label - The user friendly name.
 * @property {string} format - The format of the represented data
 */


/**
 * Data definition model.
 * @typedef {Object} StoreModel~DataModel~DataDefinition
 * @property {string[]} v - The list of values.
 * @property {StoreModel~DataModel~DataDefinition~DataItem} col
 */


/**
 * The model to represent the server data.
 * @typedef {Object} StoreModel~DataModel
 * @property {boolean} isFetching - Indicates that the data is currently fetching.
 * @property {StoreModel~DataModel~DataDefinition[]} values - List of data definitions.
 */

export function valuesSelector(data) {
  return data.col;
}

/**
 * Reducer to handle all actions related to pure server data.
 * @param {StoreModel~DataModel} state
 * @param {{ type: string, data: * }} action
 */
export default function data(state = {
  isFetching: false,
  values: [],
}, action) {
  switch (action.type) {
    case REQUEST_DASHBOARD_DATA:
      return { ...state, isFetching: true, };
    case RECEIVE_DASHBOARD_DATA:
      return { ...state, isFetching: false, values: valuesSelector(action.data), };
    /* TODO: handle errors during fetch */
    default:
      return state;
  }
}
