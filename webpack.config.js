const path = require("path");
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

  devtool: 'source-map',

  resolve: {
    extensions: ['', '.sass', '.scss', '.css', '.js', '.jsx', '.json'],
    modulesDirectories: ['node_modules'],
  },

  resolveLoader: {
    modulesDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extensions: ['', '.js'],
  },

  entry: {
    bundle: ['es6-promise', 'whatwg-fetch', './src/index.js'],
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js?[hash]',
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        include: path.resolve(__dirname, 'src'),
        loader: 'babel',
      },
      {
        test: /(\.sass|\.scss|\.css)$/,
        loaders: [
          'style',
          'css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
          'sass',
        ],
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      API_HOST_NAME: JSON.stringify('localhost:3000'),
      'process.env': {
        NODE_ENV: JSON.stringify('production'), //TODO: get from cli
      },
    }),

    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
    }),

    //TODO: ExtractTextPlugin

    new webpack.NoErrorsPlugin(),

    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: './index.html',
    }),
  ],
};
