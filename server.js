import path from 'path';
import express from 'express';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import config from './webpack.config';

const settings = {
  host: 'localhost',
  port: 3000,
};

const app = express();
const compiler = webpack(config);

app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.get('/api/dashboard/data', (req, res) => {
  res.sendFile(path.join(__dirname, 'data/data.json'));
});

app.listen(settings.port, settings.host, (err) => {
  if (err) {
    console.log(err);
    return;
  }

  console.log('Listening at http://localhost:3000');
});
