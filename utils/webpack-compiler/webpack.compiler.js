/* eslint-disable strict */
'use strict';
/* eslint-enable strict */

/**
 * Compile a source code using webpack.
 * Needed for a some modern test frameworks such as mocha or tape.
 *
 * Parameters:
 * --webpack-config - Absolute or relative path to webpack config.
 * --webpack-compiler-fs - If true emit the results to file system. Default path is 'tmp' folder in the process.pwd()
 * directory. To change folder specify 'output.path' property in the webpack config file.
 *
 * Run with tape:
 * tape -r utils/webpack-compiler --webpack-config ./webpack.tests.config.js tests/component.spec.js
 *
 * Run with mocha:
 * mocha --compilers js:utils/webpack-compiler --webpack-config ./webpack.tests.config.js tests/component.spec.js
 */
const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const merge = require('deepmerge');
const webpack = require('webpack');
const deasync = require('deasync');
const MemoryFS = require('memory-fs');

const webpackConfig = loadWebpackConfig();
const shouldCompileFile = loadFilter();

function getParameter(name) {
  const args = process.argv.slice(2);
  for (let i = 0; i < args.length; i++) {
    if (args[i] === name) {
      return args[++i];
    }
  }
  return undefined;
}

function getAbsolutePath(filePath) {
  if (!path.isAbsolute(filePath))
    return path.join(process.cwd(), filePath);
  return filePath;
}

function loadWebpackConfig() {
  const configPath = getParameter('--webpack-config');
  if (!configPath) throw new Error('Specify webpack config for compiler using \'--webpack-config\' option');
  const absoluteConfigPath = getAbsolutePath(configPath);
  return require(absoluteConfigPath);
}

function loadFilter() {
  const filterPath = getParameter('--webpack-files-filter');
  if(!filterPath) return defaultFilter;
  const absoluteFilterPath = getAbsolutePath(filterPath);
  return require(absoluteFilterPath);
}

function defaultFilter(filename) {
  return !/node_modules/.test(filename);
}

function createCompilerConfig(filename) {
  const defaultConfig = {
    target: 'node',

    output: {
      path: path.join(process.cwd(), 'tmp'),
      filename: path.parse(filename).base,
    },

    entry: {
      target: filename,
    },
  };

  return merge(defaultConfig, webpackConfig);
}

function compileSync(compiler, outPath) {
  let build = undefined;
  let done = false;

  compiler.run((err, stats) => {
    if (err) {
      console.error(err);
    } else {
      const jsonStats = stats.toJson();

      if (jsonStats.errors.length > 0) {
        console.log(jsonStats.errors);
      }

      if (jsonStats.warnings.length > 0) {
        console.log(jsonStats.warnings);
      }

      build = compiler.outputFileSystem.readFileSync(outPath, 'utf8');
      if (getParameter('--webpack-compiler-fs') === 'true') {
        mkdirp.sync(path.parse(outPath).dir);
        fs.writeFileSync(outPath, build);
      }
    }

    done = true;
  });

  /*
   * Webpack doesn't have sync version of 'compiler.run' method, so we need to a wait while done.
   */
  deasync.loopWhile(() => !done);

  return build;
}

function createCompiler(config) {
  const compiler = webpack(config);
  compiler.outputFileSystem = new MemoryFS();
  return compiler;
}

require.extensions['.js'] = function (module, filename) {
  let build = null;
  if (shouldCompileFile(filename)) {
    const config = createCompilerConfig(filename);
    const compiler = createCompiler(config);
    build = compileSync(compiler, path.join(config.output.path, config.output.filename));
  } else {
    build = fs.readFileSync(filename, 'utf8');
  }

  return module._compile(build, filename);
};
